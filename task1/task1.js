// 1. Buat array of object students ada 5 students cukup
// 2. 3 Function, 1 function nentuin provinsi kalian ada di jawa barat gak, umur kalian diatas 22 atau gak, status kalian single atau gak
// 3. Callback function untuk print hasil proses 3 function diatas.
// contoh output : nama saya Imam, saya  tinggal di Jawa Barat, umur saya dibawah 22 tahun, dan status saya single loh.



// 1. Buat array of object berjumlah 5 students
let students = [
    {
        nama : 'Deka Mersandi',
        jenis_kelamin: 'Laki-laki',
        umur : 22,
        provinsi: 'Jawa Barat',
        status: 'Single'
    },

    {
        nama : 'Selly',
        jenis_kelamin: 'Perempuan',
        umur : 16,
        provinsi: 'Jawa Timur',
        status: 'Single'
    },

    {
        nama : 'Vannissa',
        jenis_kelamin: 'Perempuan',
        umur : 26,
        provinsi: 'Jawa Tengah',
        status: 'Menikah'
    },

    {
        nama : 'Gilang Oktaverina',
        jenis_kelamin: 'Perempuan',
        umur : 22,
        provinsi: 'Jawa Barat',
        status: 'Single'
    },

    {
        nama : 'Rizki',
        jenis_kelamin: 'Laki-Laki',
        umur : 22,
        provinsi: 'Jawa Barat',
        status: 'Menikah'   
    }
]

// 2. Membuat 3 Function :  a. Function Menentukan provinsi student yang ada di Jawa Barat
//                          b. Function Umur >22 dan <22
//                          c. Function Status "Single" or "Menikah"

// a. Function Menentukan provinsi student yang ada di Jawa Barat
function studentsJabar (x, dataStudents){
    if (dataStudents[x].provinsi != 'Jawa Barat') {
        return dataStudents[x].provinsi
    } else {
        return 'Jawa Barat'
    }
}

// b. Function Umur >22 dan <22
function studentsUmur22 (x, dataStudents){
    if (dataStudents[x].umur != 22) {
        return `bukan 22 tapi ${dataStudents[x].umur}`
    } else {
        return '22'
    }
}

// c. Function Status "Single" or "Menikah"
function studentsStatus (x, dataStudents){
    if (dataStudents[x].status != 'Single') {
        return dataStudents[x].status
    } else {
        return 'Single'
    }
}

// 3. Callback function untuk print hasil proses 3 function diatas
function print(dataStudents, callback) {
    for (let i = 0; i < dataStudents.length; i++) {
        callback(dataStudents[i].nama, studentsJabar(i, dataStudents), studentsUmur22(i, dataStudents), studentsStatus(i, dataStudents));
    }

}

print(students, (nama, provinsi, umur, status) => {
    console.log(`Hay, perkenalkan namaku ${nama}, asal dari ${provinsi}, usiaku ${umur} tahun dan statusku adalah ${status}`);
})

