// 1. Bikin class Vehicle (Kendaraan)
// attribut nya (constructor): Jenis kendaraan (roda berapa 2/4 atau lainnya), negara produksi,
// ada method info yang melakukan print = 'Jenis Kendaraan roda 4 dari negara Jerman';

/**
 * Notes :
 * untuk pembuatan class awalin huruf besar
 */

// membuat kelas Vechicle
class Vehicle {
    constructor (type, productionCountry){
        this.type = type;
        this.productionCountry = productionCountry;
    }
    info() {
        console.log(`Jenis Kendaraan roda ${this.type} dari negara ${this.productionCountry}`);
    }
}

// 2. bikin child class (Mobil) dari Kendaraan, inherit attribut jenis kendaraan dan negara produksi dari super/parent class nya
// attribut baru di child class ini yaitu Merek Kendaraan, Harga Kendaraan dan Persen Pajak
// 3. ada method totalPrice yang melakukan proses menambah harga normal dengan persen pajak, yang RETURN hasil penjumlahan tersebut
// 4. overidding method info dari super/parent class (panggil instance method info dari super class = super.methodName() dan di overriding method info di child class ini ada tambahan =>
// print = 'Kendaraan ini nama mereknya Mercedes dengan total harga Rp. 880.000.000'
class Car extends Vehicle {
    constructor(type, productionCountry, brand, price, tax) {
        // memanggil Class Constructor
        super(type, productionCountry);
        // new attribute in child class
        this.brand = brand;
        this.price = price;
        this.tax = tax;
    }
    totalPrice() {
        return this.price + this.price * (this.tax / 100);
    }
    // override info method
    info() {
        super.info();
        // const getTotalPrice = this.getTotalPrice();
        console.log(`Kendaraan ini nama mereknya ${this.brand} dengan total harga Rp. ${this.totalPrice()}`);
    }
}

// 5. buat 2 instance (1 dari parent class(Vehicle), 1 dari child class aja(Mobil))
// contoh instance
const kendaraan = new Vehicle(2, 'Indonesia');
kendaraan.info();

console.log()
const mobil = new Car(4, 'Italy', 'Lamborghini', 500000000, 10);
mobil.info();

/**
    NOTES
    Rumus total price setelah di tambah pajak untuk method totalPrice, bisa kalian googling sendiri yah.
*/